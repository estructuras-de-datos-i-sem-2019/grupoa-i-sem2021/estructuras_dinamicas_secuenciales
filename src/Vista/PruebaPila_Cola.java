/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import util.ufps.colecciones_seed.Cola;
import util.ufps.colecciones_seed.Pila;

/**
 *
 * @author Invitado
 */
public class PruebaPila_Cola {
    
    
    public static void main(String[] args) {
        Pila<Integer> p=new Pila();
        Cola<Integer> c=new Cola();
        
        for(int i=1;i<=15;i++)
        {
            p.apilar(i);
            c.enColar(i);
        }
        
        System.out.println("Pila:");
        while(!p.esVacio())
            System.out.print(p.desaPilar()+"\t");
        
       System.out.println("\nCola:");
        while(!c.esVacio())
            System.out.print(c.deColar()+"\t");
    }
}
